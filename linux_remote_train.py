#-*- coding:utf-8 -*-
import subprocess
import shlex
from sklearn.grid_search import ParameterGrid
import multiprocessing
from param import *
import sys

#说明：脚本的功能，将本地的代码传到server上,然后同时在多台server上进行训练,最后将结果就进行回传

#本项目地址,需要开辟logs文件夹存放回传的log,bash文件夹存放sh文件
LOCAL_MAIN_DIR = 'd:/RemoteDSN/'
PERF_LOCAL_DIR = LOCAL_MAIN_DIR + 'logs/'
BASH_LOCAL_DIR = LOCAL_MAIN_DIR + 'bash/'

#对于每个server,预先在每台机器上，建bashfile文件夹
#我们需要将本机生成的sh文件和本地的工程传到/tmp/bashfile
REMOTE_BASH_DIR = '/tmp/bashfile/'

#待传工程名
PROJECT_NAME = 'DSN'
#在server上工程的暂存地址
TEMP_PROJECT_DIR = REMOTE_BASH_DIR +  PROJECT_NAME + '/'
#远程运行的主程序名
#PYTHON_RUN_FILE = 'deepFM_Bow_Param.py'

#在这边增加需要运行的数据预处理的脚本
PYTHON_RUN_FILE1 = 'rewrite_config.py'
PYTHON_RUN_FILE2 = 'main.py'
#本地的工程地址
PROJDECT_DIR = 'd:/RemoteDSN/DSN/'
#服务器上的目标工程地址
DEST_DIR = '/mnt/jdlu_new/codes/'


IS_DOWNLOAD_GIT_REPO = True
TMP_BASH_FILE = ''




def single_machine_run(g_list):
    if len(g_list) < 1:
        return

    g_num = len(g_list)
    current_index = 0
    name = g_list[0][1]

    TMP_BASH_FILE = PROJECT_NAME + '__' + name + '.sh'

    bash_file_content = ''
    if name in ['d3', 'd5', 'd6', 'd7', 'd8']:
        bash_file_content += 'export PATH="/home/deeprec/anaconda3/bin:$PATH" '
    elif name in ['d4']:
        bash_file_content += 'export PATH="/home/deeprec/usr/anaconda3/bin:$PATH" '

    bash_file_content += '\nsource /etc/profile'


    if IS_DOWNLOAD_GIT_REPO:
        bash_file_content += '\ncd /mnt/ \nsudo mkdir jdlu_new \nsudo chmod 777 jdlu_new '\
            + '\ncd /mnt/jdlu_new \nmkdir codes \ncd /mnt/jdlu_new/codes/ '\

        bash_file_content += '\nsudo cp -r ' + TEMP_PROJECT_DIR +' '+ DEST_DIR
        bash_file_content += '\nsudo chmod 777 '+DEST_DIR + ' '+ '-R'
        bash_file_content += '\ncd /mnt/jdlu_new/codes/' + PROJECT_NAME + '/ ' \
                             + '\nsudo rm logs/* -rf '


    az_loc = ''
    if name in ['d3', 'd4', 'd5', 'd6', 'd7', 'd8']:
        az_loc = '/home/deeprec/bin/az'


    for g, name in g_list:
        g_str = []
        for k in g:
            g_str.append(k + ' ' + str(g[k]))
        print ('{0} {2:d}:{3:d} run started on {1}'.format(name, multiprocessing.current_process().name, current_index, g_num))
        current_index += 1
        train_filename = g['--train_file'].replace('data/','')
        test_filename = g['--eval_file'].replace('data/', '')

        bash_file_content += '\ncd data \nif [ -f ' + train_filename + ' ];\nthen echo train_file exist! ;\nelse ' + az_loc + ' storage blob download-batch -s https://zhfzhmsra.blob.core.windows.net/upload/ -d ./ --pattern '\
                        +  train_filename + ' ;\nfi ;\ncd .. '
        bash_file_content += '\ncd data \nif [ -f ' + test_filename + ' ];\nthen echo eval_file exist! ;\nelse ' + az_loc + ' storage blob download-batch -s https://zhfzhmsra.blob.core.windows.net/upload/ -d ./ --pattern ' \
                        + test_filename + ' ;\nfi ;\ncd .. '
        #对于数据预处理需要的脚本
        bash_file_content += '\npython ' + PYTHON_RUN_FILE1 + ' '\
                        + ' '.join(g_str) + ' '
        bash_file_content += '\npython ' + PYTHON_RUN_FILE2


    bash_file_content += '\necho Finished'
    bash_file_content += '\n'

    #bash_file_content = 'echo zhfzh \n echo fuz \n echo antoerh'
    with open('bash/' + TMP_BASH_FILE, 'w') as f:
        f.write(bash_file_content)

    command_line = '\"D:\jdlu\Git\git-bash.exe\"' \
                   + ' -c "'\
                   + 'scp ' + BASH_LOCAL_DIR + TMP_BASH_FILE + ' ' + name + ':' + REMOTE_BASH_DIR \
                   + ' && scp -r ' + PROJDECT_DIR +' ' + name + ':' + REMOTE_BASH_DIR \
                   + '&& ssh ' + name + ' \''\
                   + 'mkdir ' + REMOTE_BASH_DIR + ' || true' \
                   + ' && cd ' + REMOTE_BASH_DIR + ' && dos2unix ' + TMP_BASH_FILE\
                   + ' && /bin/bash ' + TMP_BASH_FILE\
                   + '\''

    command_line += ' && scp ' + name + ':/mnt/jdlu_new/codes/' + PROJECT_NAME + '/logs/* ' \
                    + PERF_LOCAL_DIR

    #command_line += ' && /usr/bin/bash --login -i'
    command_line += '"'

    print (command_line)
    args = shlex.split(command_line)
    gitbash_process = subprocess.Popen(args)
    output = gitbash_process.wait()
    print ('{0} run finished'.format(g_list[0][1]))

def start_process():
    print ('Starting {0}'.format(multiprocessing.current_process().name))


if __name__ == '__main__':
    #linux_machines = ['d4', 'd5', 'd6', 'd7', 'd8']
    #传数据需要一个个传递
    linux_machines = ['d3', 'd4', 'd5', 'd6', 'd7'] #
    #linux_machines = ['d3', 'd4', 'd5']
    linux_machines_num = len(linux_machines)
    grid_searches = ParameterGrid(PARAMS['model_param'])
    allocates = []


    for i in range(linux_machines_num):
        allocates.append([])

    index = 0
    for g in grid_searches:
        allocates[index % linux_machines_num].append((g, linux_machines[index % linux_machines_num]))
        index += 1

    pool_size = linux_machines_num
    pool = multiprocessing.Pool(processes=pool_size,
                                initializer=start_process, )

    pool_outputs = pool.map(single_machine_run, allocates)
    pool.close()
    pool.join()
    print ('Finished')
