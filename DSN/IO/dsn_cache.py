"""define deep interest network cache class for reading data"""
from IO.base_cache import BaseCache
import tensorflow as tf
import numpy as np
from collections import defaultdict
import utils.util as util

__all__ = ["DsnCache"]


# word index start by 1
# 每个document的词数是一致的,次数可以设为L = 5
class DsnCache(BaseCache):
    def _load_batch_data_from_file(self, file, hparams):
        batch_size = hparams.batch_size
        labels = []
        candidate_news_index_batch = []
        candidate_news_val_batch = []
        click_news_index_batch = []
        click_news_val_batch = []
        click_doc_index_batch = []
        click_doc_val_batch = []
        impression_id = []
        cnt = 0
        with open(file, 'r') as rd:
            while True:
                line = rd.readline().strip(' ')
                if not line:
                    break
                tmp = line.strip().split(util.USER_ID_SPLIT)
                if len(tmp) == 2:
                    impression_id.append(tmp[1].strip())
                line = tmp[0]
                cnt += 1
                cols = line.strip().split(' ')
                label = float(cols[0])
                if label > 0:
                    label = 1
                else:
                    label = 0
                candidate_news_index = []
                candidate_news_val = []
                click_news_index = []
                click_news_val = []
                click_doc_index = []
                click_doc_val = []
                for word in cols[1:]:
                    if not word:
                        continue
                    tokens = word.split(':')
                    if tokens[0].strip() == 'CandidateNews':
                        # word index start by 0
                        for item in tokens[1].strip().split(','):
                            candidate_news_index.append(int(item))
                            candidate_news_val.append(float(1))
                    elif 'clickedNews' in tokens[0].strip():
                        for item in tokens[1].strip().split(','):
                            click_news_index.append(int(item))
                            click_news_val.append(float(1))
                    elif 'clickedDoc' in tokens[0].strip():
                        for item in tokens[1].strip().split(','):
                            click_doc_index.append(int(item))
                            click_doc_val.append(float(1))
                    else:
                        raise ValueError("data format is wrong")

                candidate_news_index_batch.append(candidate_news_index)
                candidate_news_val_batch.append(candidate_news_val)
                click_news_index_batch.append(click_news_index)
                click_news_val_batch.append(click_news_val)
                click_doc_index_batch.append(click_doc_index)
                click_doc_val_batch.append(click_doc_val)
                labels.append(label)

                if cnt == batch_size:
                    yield labels, candidate_news_index_batch, candidate_news_val_batch, \
                          click_news_index_batch, click_news_val_batch, \
                          click_doc_index_batch, click_doc_val_batch
                    labels = []
                    candidate_news_index_batch = []
                    candidate_news_val_batch = []
                    click_news_index_batch = []
                    click_news_val_batch = []
                    click_doc_index_batch = []
                    click_doc_val_batch = []
                    cnt = 0
        #最后不足batch_size的数据丢弃掉，为了代码简单化
        # if cnt > 0:
        #     yield labels, candidate_news_index_batch, candidate_news_val_batch, \
        #           click_news_index_batch, click_news_val_batch, \
        #           click_doc_index_batch, click_doc_val_batch
        #     labels = []
        #     candidate_news_index_batch = []
        #     candidate_news_val_batch = []
        #     click_news_index_batch = []
        #     click_news_val_batch = []
        #     click_doc_index_batch = []
        #     click_doc_val_batch = []
        #     cnt = 0

    def _convert_data(self, labels, candidate_news_index_batch, candidate_news_val_batch, \
                      click_news_index_batch, click_news_val_batch, \
                      click_doc_index_batch, click_doc_val_batch ,hparams):
        batch_size = hparams.batch_size
        instance_cnt = len(labels)

        click_news_indices = []
        click_news_values = []
        click_news_weights = []
        click_news_shape = [batch_size, -1]

        #处理click_field_shape的数据,构造稀疏的矩阵来进行存储
        batch_max_len = 0
        for i in range(instance_cnt):
            m = len(click_news_index_batch[i])
            batch_max_len = m if m>batch_max_len else batch_max_len
            for j in range(m):
                click_news_indices.append([i, j])
                click_news_values.append(click_news_index_batch[i][j])
                click_news_weights.append(click_news_val_batch[i][j])
        click_news_shape[1] = batch_max_len

        click_doc_indices = []
        click_doc_values = []
        click_doc_weights = []
        click_doc_shape = [batch_size, -1]

        batch_max_len = 0
        for i in range(instance_cnt):
            m = len(click_doc_index_batch[i])
            batch_max_len = m if m>batch_max_len else batch_max_len
            for j in range(m):
                click_doc_indices.append([i, j])
                click_doc_values.append(click_doc_index_batch[i][j])
                click_doc_weights.append(click_doc_val_batch[i][j])
        click_doc_shape[1] = batch_max_len

        res = {}
        res['labels'] = np.asarray([[label] for label in labels], dtype=np.float32)
        res['candidate_news_index_batch'] = np.asarray(candidate_news_index_batch, dtype=np.int64)
        res['candidate_news_val_batch'] = np.asarray(candidate_news_val_batch, dtype=np.float32)
        res['click_news_indices'] = np.asarray(click_news_indices, dtype=np.int64)
        res['click_news_values'] = np.asarray(click_news_values, dtype=np.int64)
        res['click_news_weights'] = np.asarray(click_news_weights, dtype=np.float32)
        res['click_news_shape'] = np.asarray(click_news_shape, dtype=np.int64)

        res['click_doc_indices'] = np.asarray(click_doc_indices, dtype=np.int64)
        res['click_doc_values'] = np.asarray(click_doc_values, dtype=np.int64)
        res['click_doc_weights'] = np.asarray(click_doc_weights, dtype=np.float32)
        res['click_doc_shape'] = np.asarray(click_doc_shape, dtype=np.int64)
        return res

    def write_tfrecord(self, infile, outfile, hparams):
        writer = tf.python_io.TFRecordWriter(outfile)
        for index, (labels, candidate_news_index_batch, candidate_news_val_batch, \
                    click_news_index_batch, click_news_val_batch, \
                    click_doc_index_batch, click_doc_val_batch) \
                    in enumerate(self._load_batch_data_from_file(infile, hparams)):

            input_in_sp = self._convert_data(labels, candidate_news_index_batch, candidate_news_val_batch, \
                      click_news_index_batch, click_news_val_batch, click_doc_index_batch, click_doc_val_batch ,hparams)

            labels = input_in_sp['labels']
            candidate_news_index_batch = input_in_sp['candidate_news_index_batch']
            candidate_news_val_batch = input_in_sp['candidate_news_val_batch']

            click_news_indices = input_in_sp['click_news_indices']
            click_news_values = input_in_sp['click_news_values']
            click_news_weights = input_in_sp['click_news_weights']
            click_news_shape = input_in_sp['click_news_shape']

            click_doc_indices = input_in_sp['click_doc_indices']
            click_doc_values = input_in_sp['click_doc_values']
            click_doc_weights = input_in_sp['click_doc_weights']
            click_doc_shape = input_in_sp['click_doc_shape']

            candidate_news_index_batch_str = candidate_news_index_batch.tostring()
            candidate_news_val_batch_str = candidate_news_val_batch.tostring()
            labels_str = labels.tostring()
            click_news_indices_str = click_news_indices.tostring()
            click_doc_indices_str = click_doc_indices.tostring()

            example = tf.train.Example(
                features=tf.train.Features(
                    feature={
                        'candidate_news_index_batch': tf.train.Feature(
                            bytes_list=tf.train.BytesList(value=[candidate_news_index_batch_str])),
                        'candidate_news_val_batch': tf.train.Feature(
                            bytes_list=tf.train.BytesList(value=[candidate_news_val_batch_str])),
                        'labels': tf.train.Feature(
                            bytes_list=tf.train.BytesList(value=[labels_str])),
                        'click_news_indices': tf.train.Feature(
                            bytes_list=tf.train.BytesList(value=[click_news_indices_str])),
                        'click_news_values': tf.train.Feature(
                            int64_list=tf.train.Int64List(value=click_news_values)),
                        'click_news_weights': tf.train.Feature(
                            float_list=tf.train.FloatList(value=click_news_weights)),
                        'click_news_shape': tf.train.Feature(
                            int64_list=tf.train.Int64List(value=click_news_shape)),
                        'click_doc_indices': tf.train.Feature(
                            bytes_list=tf.train.BytesList(value=[click_doc_indices_str])),
                        'click_doc_values': tf.train.Feature(
                            int64_list=tf.train.Int64List(value=click_doc_values)),
                        'click_doc_weights': tf.train.Feature(
                            float_list=tf.train.FloatList(value=click_doc_weights)),
                        'click_doc_shape': tf.train.Feature(
                            int64_list=tf.train.Int64List(value=click_doc_shape))
                    }
                )
            )
            serialized = example.SerializeToString()
            writer.write(serialized)

def test_cache():
    def create_hparams():
        """Create hparams."""
        return tf.contrib.training.HParams(
            batch_size=2
        )

    hparams = create_hparams()
    infile = '../data/train.all.norm.fieldwise.toy.txt'
    outfile = '../cache/train.all.norm.fieldwise.toy.tfrecord'
    cache = DsnCache()
    cache.write_tfrecord(infile, outfile, hparams)

# test_cache()
