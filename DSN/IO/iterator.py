"""define iterator"""
import collections
import tensorflow as tf
import abc

BUFFER_SIZE = 256
__all__ = ["BaseIterator", "DsnIterator", "DsnSingleChannelIterator"]


class BaseIterator(object):
    @abc.abstractmethod
    def get_iterator(self, src_dataset):
        """Subclass must implement this."""
        pass

    @abc.abstractmethod
    def parser(self, record):
        pass


class DsnIterator(BaseIterator):
    def __init__(self, src_dataset, hparams):
        self.batch_size = hparams.batch_size
        self.doc_size = hparams.doc_size
        self.get_iterator(src_dataset)

    def get_iterator(self, src_dataset):
        src_dataset = src_dataset.map(self.parser)
        # shuffle
        # src_dataset = src_dataset.shuffle(buffer_size=BUFFER_SIZE)
        iterator = src_dataset.make_initializable_iterator()
        _candidate_news_index_batch, _candidate_news_val_batch, _labels, \
        _click_news_indices, _click_news_values, _click_news_weights, _click_news_shape, \
        _click_doc_indices, _click_doc_values, _click_doc_weights, _click_doc_shape = iterator.get_next()
        self.initializer = iterator.initializer
        self.candidate_news_index_batch = _candidate_news_index_batch
        self.candidate_news_val_batch = _candidate_news_val_batch
        self.labels = _labels
        self.click_news_indices = _click_news_indices
        self.click_news_values = _click_news_values
        self.click_news_weights = _click_news_weights
        self.click_news_shape = _click_news_shape

        self.click_doc_indices = _click_doc_indices
        self.click_doc_values = _click_doc_values
        self.click_doc_weights = _click_doc_weights
        self.click_doc_shape = _click_doc_shape

    def parser(self, record):
        keys_to_features = {
            'candidate_news_index_batch': tf.FixedLenFeature([], tf.string),
            'candidate_news_val_batch': tf.FixedLenFeature([], tf.string),
            'labels': tf.FixedLenFeature([], tf.string),
            'click_news_indices': tf.FixedLenFeature([], tf.string),
            'click_news_values': tf.VarLenFeature(tf.int64),
            'click_news_weights': tf.VarLenFeature(tf.float32),
            'click_news_shape': tf.FixedLenFeature([2], tf.int64),
            'click_doc_indices': tf.FixedLenFeature([], tf.string),
            'click_doc_values': tf.VarLenFeature(tf.int64),
            'click_doc_weights': tf.VarLenFeature(tf.float32),
            'click_doc_shape': tf.FixedLenFeature([2], tf.int64)
        }

        parsed = tf.parse_single_example(record, keys_to_features)
        candidate_news_index_batch = tf.reshape(tf.decode_raw(parsed['candidate_news_index_batch'], tf.int64),
                                                [self.batch_size, self.doc_size])
        candidate_news_val_batch = tf.reshape(tf.decode_raw(parsed['candidate_news_val_batch'], tf.float32),
                                              [self.batch_size, self.doc_size])
        labels = tf.reshape(tf.decode_raw(parsed['labels'], tf.float32), [-1, 1])
        click_news_indices = tf.reshape(tf.decode_raw(parsed['click_news_indices'], tf.int64), [-1, 2])
        click_news_values = tf.sparse_tensor_to_dense(parsed['click_news_values'])
        click_news_weights = tf.sparse_tensor_to_dense(parsed['click_news_weights'])
        click_news_shape = parsed['click_news_shape']
        click_doc_indices = tf.reshape(tf.decode_raw(parsed['click_doc_indices'], tf.int64), [-1, 2])
        click_doc_values = tf.sparse_tensor_to_dense(parsed['click_doc_values'])
        click_doc_weights = tf.sparse_tensor_to_dense(parsed['click_doc_weights'])
        click_doc_shape = parsed['click_doc_shape']

        return candidate_news_index_batch, candidate_news_val_batch, labels, \
               click_news_indices, click_news_values, \
               click_news_weights, click_news_shape, \
               click_doc_indices, click_doc_values, \
               click_doc_weights, click_doc_shape


class DsnSingleChannelIterator(BaseIterator):
    def __init__(self, src_dataset, hparams):
        self.batch_size = hparams.batch_size
        self.doc_size = hparams.doc_size
        self.get_iterator(src_dataset)

    def get_iterator(self, src_dataset):
        src_dataset = src_dataset.map(self.parser)
        # shuffle
        # src_dataset = src_dataset.shuffle(buffer_size=BUFFER_SIZE)
        iterator = src_dataset.make_initializable_iterator()
        _candidate_news_index_batch, _candidate_news_val_batch, _labels, \
        _click_news_indices, _click_news_values, _click_news_weights, _click_news_shape = iterator.get_next()
        self.initializer = iterator.initializer
        self.candidate_news_index_batch = _candidate_news_index_batch
        self.candidate_news_val_batch = _candidate_news_val_batch
        self.labels = _labels
        self.click_news_indices = _click_news_indices
        self.click_news_values = _click_news_values
        self.click_news_weights = _click_news_weights
        self.click_news_shape = _click_news_shape

    def parser(self, record):
        keys_to_features = {
            'candidate_news_index_batch': tf.FixedLenFeature([], tf.string),
            'candidate_news_val_batch': tf.FixedLenFeature([], tf.string),
            'labels': tf.FixedLenFeature([], tf.string),
            'click_news_indices': tf.FixedLenFeature([], tf.string),
            'click_news_values': tf.VarLenFeature(tf.int64),
            'click_news_weights': tf.VarLenFeature(tf.float32),
            'click_news_shape': tf.FixedLenFeature([2], tf.int64)
        }

        parsed = tf.parse_single_example(record, keys_to_features)
        candidate_news_index_batch = tf.reshape(tf.decode_raw(parsed['candidate_news_index_batch'], tf.int64),
                                                [self.batch_size, self.doc_size])
        candidate_news_val_batch = tf.reshape(tf.decode_raw(parsed['candidate_news_val_batch'], tf.float32),
                                              [self.batch_size, self.doc_size])
        labels = tf.reshape(tf.decode_raw(parsed['labels'], tf.float32), [-1, 1])
        click_news_indices = tf.reshape(tf.decode_raw(parsed['click_news_indices'], tf.int64), [-1, 2])
        click_news_values = tf.sparse_tensor_to_dense(parsed['click_news_values'])
        click_news_weights = tf.sparse_tensor_to_dense(parsed['click_news_weights'])
        click_news_shape = parsed['click_news_shape']

        return candidate_news_index_batch, candidate_news_val_batch, labels, \
               click_news_indices, click_news_values, \
               click_news_weights, click_news_shape

    # 测试代码


# def test_DsnIterator():
#
#     def create_hparams():
#         """Create hparams."""
#         return tf.contrib.training.HParams(
#             batch_size=2,
#             doc_size = 5
#         )
#     hparams = create_hparams()
#     outfile = '../cache/train.all.norm.fieldwise.toy.tfrecord'
#     filenames = [outfile]
#     src_dataset = tf.contrib.data.TFRecordDataset(filenames)
#     batch_iter = DsnIterator(src_dataset, hparams)
#     sess = tf.Session()
#     for _ in range(2):
#         sess.run(batch_iter.initializer)
#         while True:
#             try:
#                 print(sess.run([batch_iter.candidate_news_index_batch, batch_iter.candidate_news_val_batch, batch_iter.labels, \
#                                 batch_iter.click_news_indices, batch_iter.click_news_values, batch_iter.click_news_weights, batch_iter.click_news_shape, \
#                                 batch_iter.click_doc_indices, batch_iter.click_doc_values, batch_iter.click_doc_weights, batch_iter.click_doc_shape
#                                 ]))
#                 break
#             except tf.errors.OutOfRangeError:
#                 print("end")
#                 break


# test_DsnIterator()

def test_DsnSingleChannelIterator():
    def create_hparams():
        """Create hparams."""
        return tf.contrib.training.HParams(
            batch_size=2,
            doc_size=5
        )

    hparams = create_hparams()
    # outfile = '../cache/train.all.norm.fieldwise.toy.tfrecord'
    outfile = './batch_size_128_model_dsnSingleChannel_train_dsn_news.tfrecord'
    filenames = [outfile]
    src_dataset = tf.contrib.data.TFRecordDataset(filenames)
    batch_iter = DsnSingleChannelIterator(src_dataset, hparams)
    sess = tf.Session()
    for _ in range(2):
        sess.run(batch_iter.initializer)
        total = 0
        while True:
            try:
                # print(sess.run(
                #     [batch_iter.candidate_news_index_batch, batch_iter.candidate_news_val_batch, batch_iter.labels, \
                #      batch_iter.click_news_indices, batch_iter.click_news_values, \
                #      batch_iter.click_news_weights, batch_iter.click_news_shape
                #      ]))
                label = sess.run(batch_iter.labels)
                total += len(label)
                break
            except tf.errors.OutOfRangeError:
                print('sample num:', total)
                print("end")
                break


# test_DsnSingleChannelIterator()
