import sys


def analyze(filename):
    featSet = set()
    with open(filename, 'r') as f:
        for line in f:
            tmp = line.strip().split(' ')
            for news in tmp[1:]:
                for item in news.split(':')[1].split(','):
                    featSet.add(int(item))
    print('process file name:', filename)
    print('min feat index:', min(featSet))
    print('max feat index:', max(featSet))


filename = './toy'
analyze(filename)

filename = './toy'
analyze(filename)
