"""define util function and  global variable"""
import tensorflow as tf
import os, sys
import json, codecs
import time, yaml

# 项目内,必须存在着三个文件夹
RES_DIR = './res/'  # 存放预测结果
CACHE_DIR = './cache/'  # 存放数据的缓存,tfrecord格式
MODEL_DIR = './checkpoint/'  # 存放model
CONFIG_DIR = './config/'
TRAIN_YAML = 'network.yaml'
TRAIN_NUM = './cache/train_num.csv'
EVAL_NUM = './cache/eval_num.csv'
INFER_NUM = './cache/infer_num.csv'
LOG_DIR = './logs/'
FEAT_COUNT_FILE = './cache/feat_cnt.csv'
TRAIN_IMPRESSION_ID = './cache/train_impressionId.csv'
EVAL_IMPRESSION_ID = './cache/eval_impressionId.csv'
INFER_IMPRESSION_ID = './cache/infer_impressionId.csv'
SUMMARIES_DIR = './logs/'
DIN_FORMAT_SPLIT = '#'#DIN的数据格式使用#隔开
USER_ID_SPLIT = '%'#userid和特征文件使用%隔开

def check_and_mkdir():
    def make_dir(DIR):
        if not os.path.exists(DIR):
            os.mkdir(DIR)
    make_dir(RES_DIR)
    make_dir(CACHE_DIR)
    make_dir(MODEL_DIR)
    make_dir(CONFIG_DIR)
    make_dir(LOG_DIR)


def check_tensorflow_version():
    if tf.__version__ < "1.2.0":
        raise EnvironmentError("Tensorflow version must >= 1.2.0,but version is {0}".\
                               format(tf.__version__))

def print_time(s, start_time):
    """Take a start time, print elapsed duration, and return a new time."""
    print("%s, time %ds, %s." % (s, (time.time() - start_time), time.ctime()))
    sys.stdout.flush()
    return time.time()

def check_file_exist(filename):
    if not os.path.isfile(filename):
        raise ValueError("{0} is not exits".format(filename))


def load_yaml_file(filename):
    with open(filename) as f:
        try:
            config = yaml.load(f)
            print(config)
        except:
            raise IOError("load {0} error!".format(filename))
    return config


def convert_cached_name(file_name, batch_size, model_type):
    prefix = CACHE_DIR + 'batch_size_' + str(batch_size) + '_model_' + str(model_type) + '_'
    prefix += (file_name.strip().split('/'))[-1]
    train_cache_name = prefix.replace(".txt", ".tfrecord").\
                              replace(".csv", ".tfrecord").\
                              replace(".libsvm", ".tfrecord")
    return train_cache_name


def convert_res_name(file_name):
    prefix = RES_DIR
    inferfile = file_name.split('/')[-1]
    res_name = prefix + inferfile.replace("tfrecord", "res.csv").\
                                  replace(".csv", ".tfrecord").\
                                  replace(".libsvm", ".tfrecord")
    return res_name


# 将参数hparams解析压缩到json文件中
def save_hparams(out_dir, hparams):
    """Save hparams."""
    hparams_file = os.path.join(out_dir, "hparams")
    print("  saving hparams to %s" % hparams_file)
    with codecs.getwriter("utf-8")(tf.gfile.GFile(hparams_file, "wb")) as f:
        f.write(hparams.to_json())


# 将参数从json文件中解析出来
def load_hparams(model_dir):
    """Load hparams from an existing model directory."""
    hparams_file = os.path.join(model_dir, "hparams")
    if tf.gfile.Exists(hparams_file):
        print("# Loading hparams from %s" % hparams_file)
        with codecs.getreader("utf-8")(tf.gfile.GFile(hparams_file, "rb")) as f:
            try:
                hparams_values = json.load(f)
                hparams = tf.contrib.training.HParams(**hparams_values)
            except ValueError:
                print("  can't load hparams file")
                return None
        return hparams
    else:
        return None

# 输出hparams的值
def print_hparams(hparams):
    values = hparams.values()
    for key in sorted(values.keys()):
        print("  %s=%s" % (key, str(values[key])))
