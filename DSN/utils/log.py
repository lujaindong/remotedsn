"""define logging configure"""
import logging
import utils.util as util
from datetime import datetime, timedelta, timezone
import platform

#UTC To Beijing Time
utc_dt = datetime.utcnow().replace(tzinfo=timezone.utc)
bj_dt = utc_dt.astimezone(timezone(timedelta(hours=8)))

logging_filename = 'logs/' + platform.node() + '__' + bj_dt.strftime('%Y-%m-%d_%H_%M_%S') + '.log'
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
handler = logging.FileHandler(logging_filename)
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)