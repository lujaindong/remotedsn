"""This script parse and run train function"""
import train
import utils.util as util
import tensorflow as tf
import sys

def flat_config(config):
    """flat config to a dict"""
    f_config = {}
    category = ['data', 'model', 'train', 'info']
    for cate in category:
        for key, val in config[cate].items():
            f_config[key] = val
    return f_config

def create_hparams(FLAGS):
    """Create hparams."""
    FLAGS = flat_config(FLAGS)
    return tf.contrib.training.HParams(
        # data
        train_file=FLAGS['train_file'] if 'train_file' in FLAGS else None,
        eval_file=FLAGS['eval_file'] if 'eval_file' in FLAGS else None,
        infer_file=FLAGS['infer_file'] if 'infer_file' in FLAGS else None,
        word_size=FLAGS['word_size'] if 'word_size' in FLAGS else None,
        doc_size=FLAGS['doc_size'] if 'doc_size' in FLAGS else None,

        # model
        model_type=FLAGS['model_type'] if 'model_type' in FLAGS else None,
        layer_sizes=FLAGS['layer_sizes'] if 'layer_sizes' in FLAGS else None,
        activation=FLAGS['activation'] if 'activation' in FLAGS else None,
        dropout=FLAGS['dropout'] if 'dropout' in FLAGS else None,
        attention_layer_sizes=FLAGS['attention_layer_sizes'] \
            if 'attention_layer_sizes' in FLAGS else FLAGS['dim'],
        attention_activation=FLAGS['attention_activation'] \
            if 'attention_activation' in FLAGS else 'relu',
        attention_dropout=FLAGS['attention_dropout'] \
            if 'attention_dropout' in FLAGS else 0.0,
        load_model_name=FLAGS['load_model_name'] \
            if 'load_model_name' in FLAGS else None,
        filter_sizes=FLAGS['filter_sizes'] if 'filter_sizes' in FLAGS else None,
        num_filters=FLAGS['num_filters'] if 'num_filters' in FLAGS else None,
        dim=FLAGS['dim'] if 'dim' in FLAGS else None,

        # train
        init_method=FLAGS['init_method'] if 'init_method' in FLAGS else 'tnormal',
        init_value=FLAGS['init_value'] if 'init_value' in FLAGS else 0.01,
        embed_l2=FLAGS['embed_l2'] if 'embed_l2' in FLAGS else 0.0000,
        embed_l1=FLAGS['embed_l1'] if 'embed_l1' in FLAGS else 0.0000,
        layer_l2=FLAGS['layer_l2'] if 'layer_l2' in FLAGS else 0.0000,
        layer_l1=FLAGS['layer_l1'] if 'layer_l1' in FLAGS else 0.0000,
        learning_rate=FLAGS['learning_rate'] if 'learning_rate' in FLAGS else 0.001,
        loss=FLAGS['loss'] if 'loss' in FLAGS else None,
        optimizer=FLAGS['optimizer'] if 'optimizer' in FLAGS else 'adam',
        epochs=FLAGS['epochs'] if 'epochs' in FLAGS else 10,
        batch_size=FLAGS['batch_size'] if 'batch_size' in FLAGS else 1,

        # show info
        show_step=FLAGS['show_step'] if 'show_step' in FLAGS else 1,
        metrics=FLAGS['metrics'] if 'metrics' in FLAGS else None
    )

# train process load yaml
def load_yaml():
    """load config from yaml"""
    yaml_name = util.CONFIG_DIR + util.TRAIN_YAML
    print('trainging network configuration file is {0}'.format(yaml_name))
    util.check_file_exist(yaml_name)
    config = util.load_yaml_file(yaml_name)
    return config


def main():
    """main function"""
    # flag = True
    config = load_yaml()
    print(config)
    #check_config(config)
    hparams = create_hparams(config)
    print(hparams.values())
    train.train(hparams)


main()
