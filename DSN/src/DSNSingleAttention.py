"""define DSN Single Attention Model"""
import math
import tensorflow as tf
from src.base_model import BaseModel

__all__ = ["DSNSingleAttentionModel"]


class DSNSingleAttentionModel(BaseModel):
    """define DSN Single Attention Model"""

    def _build_graph(self, hparams):
        with tf.variable_scope("DSNSingleAttentionModel") as scope:
            logit = self._build_dsn(hparams)
            return logit

    def _build_dsn(self, hparams):
        # build attention model for clicked news and candidate news
        click_news_embed_batch, candidate_news_embed_batch = self._build_pair_attention(
            self.iterator.click_news_indices, \
            self.iterator.click_news_values, \
            self.iterator.click_news_shape, \
            hparams, flag='click_news')
        # build attention model for clicked docs and candidate news
        click_doc_embed_batch, candidate_news_embed_batch = self._build_pair_attention(
            self.iterator.click_doc_indices, \
            self.iterator.click_doc_values, \
            self.iterator.click_doc_shape, \
            hparams, flag='click_doc')
        # projection part
        transfer_matrix = tf.get_variable(name="transfer_matrix",
                                          shape=[self.num_filters_total, self.num_filters_total], \
                                          initializer=self.initializer)
        self.layer_params.append(transfer_matrix)
        transfer_click_doc_embed_batch = tf.matmul(click_doc_embed_batch, transfer_matrix)
        nn_input = tf.concat([click_news_embed_batch, transfer_click_doc_embed_batch, candidate_news_embed_batch],
                             axis=1)

        # self.num_filters_total是kims cnn output dimension
        dnn_channel_part = 3
        last_layer_size = dnn_channel_part * self.num_filters_total
        layer_idx = 0
        hidden_nn_layers = []
        hidden_nn_layers.append(nn_input)
        with tf.variable_scope("nn_part", initializer=self.initializer) as scope:
            for idx, layer_size in enumerate(hparams.layer_sizes):
                curr_w_nn_layer = tf.get_variable(name='w_nn_layer' + str(layer_idx),
                                                  shape=[last_layer_size, layer_size],
                                                  dtype=tf.float32)
                curr_b_nn_layer = tf.get_variable(name='b_nn_layer' + str(layer_idx),
                                                  shape=[layer_size],
                                                  dtype=tf.float32)
                curr_hidden_nn_layer = tf.nn.xw_plus_b(hidden_nn_layers[layer_idx],
                                                       curr_w_nn_layer,
                                                       curr_b_nn_layer)
                scope = "nn_part" + str(idx)
                dropout = hparams.dropout[idx]
                activation = hparams.activation[idx]
                curr_hidden_nn_layer = self._active_layer(logit=curr_hidden_nn_layer,
                                                          scope=scope,
                                                          activation=activation,
                                                          dropout=dropout)
                hidden_nn_layers.append(curr_hidden_nn_layer)
                layer_idx += 1
                last_layer_size = layer_size
                self.layer_params.append(curr_w_nn_layer)
                self.layer_params.append(curr_b_nn_layer)

            w_nn_output = tf.get_variable(name='w_nn_output', shape=[last_layer_size, 1],
                                          dtype=tf.float32)
            b_nn_output = tf.get_variable(name='b_nn_output', shape=[1], dtype=tf.float32)
            if w_nn_output not in self.layer_params:
                self.layer_params.append(w_nn_output)
            if b_nn_output not in self.layer_params:
                self.layer_params.append(b_nn_output)
            nn_output = tf.nn.xw_plus_b(hidden_nn_layers[-1], w_nn_output, b_nn_output)
            return nn_output

    # build attention network
    def _build_pair_attention(self, field_indices, field_values, field_shape, hparams, flag):
        doc_size = hparams.doc_size
        attention_hidden_sizes = hparams.attention_layer_sizes
        news_field_word_batch = self.iterator.candidate_news_index_batch
        click_field_word_batch = tf.SparseTensor(field_indices, field_values, field_shape)
        click_field_word_splited = tf.sparse_split(axis=0, num_split=hparams.batch_size, \
                                                   sp_input=click_field_word_batch)
        news_field_word_splited = tf.split(axis=0, num_or_size_splits=hparams.batch_size, \
                                           value=news_field_word_batch)
        click_field_embed_final_batch = []
        news_field_embed_final_batch = []
        # 对于2个通道而言,CNN的卷积和是共享的
        # 但是attention网络是不一样的
        # 对kims cnn使用一个变量作用域
        with tf.variable_scope("kims_cnn") as kcnn_scope:
            pass
        # attention的网络参数必须是共享的
        with tf.variable_scope("attention_net", initializer=self.initializer) as scope:
            if flag == 'click_doc':
                scope.reuse_variables()
            # 只能对batch中的每个样本进行循环
            for index, news_field_word in enumerate(news_field_word_splited):
                click_field_word = click_field_word_splited[index]
                # get non-zero val
                click_field_word = click_field_word.values
                click_field_word = tf.reshape(click_field_word, [-1, doc_size])
                # use kims cnn to get conv embedding
                with tf.variable_scope(kcnn_scope, initializer=self.initializer) as cnn_scope:
                    # CNN卷积的参数都是共享的,因此这边的判断条件比较复杂
                    if index > 0 or flag == 'click_doc':
                        cnn_scope.reuse_variables()
                    news_field_embed = self._kims_cnn(news_field_word, hparams)
                    cnn_scope.reuse_variables()
                    click_field_embed = self._kims_cnn(click_field_word, hparams)

                news_field_embed_repeat = tf.add(tf.zeros_like(click_field_embed), news_field_embed)
                attention_x = tf.concat(axis=1, values=[click_field_embed, news_field_embed_repeat])
                attention_w = tf.get_variable(name="attention_hidden_w", \
                                              shape=[self.num_filters_total * 2, attention_hidden_sizes], \
                                              dtype=tf.float32)
                attention_b = tf.get_variable(name="attention_hidden_b", \
                                              shape=[attention_hidden_sizes], \
                                              dtype=tf.float32)
                curr_attention_layer = tf.nn.xw_plus_b(attention_x, attention_w, attention_b)
                dropout = hparams.attention_dropout
                activation = hparams.attention_activation
                curr_attention_layer = self._active_layer(logit=curr_attention_layer, \
                                                          scope="attention", \
                                                          activation=activation, \
                                                          dropout=dropout)
                attention_output_w = tf.get_variable(name="attention_output_w", \
                                                     shape=[attention_hidden_sizes, 1], \
                                                     dtype=tf.float32)
                attention_output_b = tf.get_variable(name="attention_output_b", \
                                                     shape=[1], \
                                                     dtype=tf.float32)
                attention_weight = tf.nn.sigmoid(
                    tf.nn.xw_plus_b(curr_attention_layer, \
                                    attention_output_w, \
                                    attention_output_b))

                click_field_embed_final = tf.reduce_sum(tf.multiply(click_field_embed, attention_weight), axis=0,
                                                        keep_dims=True)

                news_field_embed_final_batch.append(news_field_embed)
                click_field_embed_final_batch.append(click_field_embed_final)
                scope.reuse_variables()
                if attention_w not in self.layer_params:
                    self.layer_params.append(attention_w)
                if attention_b not in self.layer_params:
                    self.layer_params.append(attention_b)
                if attention_output_w not in self.layer_params:
                    self.layer_params.append(attention_output_w)
                if attention_output_b not in self.layer_params:
                    self.layer_params.append(attention_output_b)

        click_field_embed_final_batch = tf.concat(click_field_embed_final_batch, axis=0)
        news_field_embed_final_batch = tf.concat(news_field_embed_final_batch, axis=0)

        return click_field_embed_final_batch, news_field_embed_final_batch

    # variable scope统一使用同一个初始化方式
    def _kims_cnn(self, input, hparams):
        # kims cnn parameter
        filter_sizes = hparams.filter_sizes
        num_filters = hparams.num_filters
        dim = hparams.dim

        embedded_chars = tf.nn.embedding_lookup(self.embedding, input)
        embedded_chars_expanded = tf.expand_dims(embedded_chars, -1)
        # Create a convolution + maxpool layer for each filter size
        pooled_outputs = []
        for i, filter_size in enumerate(filter_sizes):
            with tf.variable_scope("conv-maxpool-%s" % filter_size, initializer=self.initializer):
                # Convolution Layer
                filter_shape = [filter_size, dim, 1, num_filters]
                W = tf.get_variable(name='W' + '_filter_size_' + str(filter_size), shape=filter_shape, dtype=tf.float32)
                b = tf.get_variable(name='b' + '_filter_size_' + str(filter_size), shape=[num_filters],
                                    dtype=tf.float32)
                if W not in self.layer_params:
                    self.layer_params.append(W)
                if b not in self.layer_params:
                    self.layer_params.append(b)
                conv = tf.nn.conv2d(
                    embedded_chars_expanded,
                    W,
                    strides=[1, 1, 1, 1],
                    padding="VALID",
                    name="conv")
                # Apply nonlinearity
                h = tf.nn.relu(tf.nn.bias_add(conv, b), name="relu")
                # Maxpooling over the outputs
                pooled = tf.nn.max_pool(
                    h,
                    ksize=[1, hparams.doc_size - filter_size + 1, 1, 1],
                    strides=[1, 1, 1, 1],
                    padding='VALID',
                    name="pool")
                pooled_outputs.append(pooled)
        # Combine all the pooled features
        # self.num_filters_total is the kims cnn output dimension
        self.num_filters_total = num_filters * len(filter_sizes)
        h_pool = tf.concat(pooled_outputs, 3)
        h_pool_flat = tf.reshape(h_pool, [-1, self.num_filters_total])
        return h_pool_flat
