# -*- coding: utf-8 -*-
import argparse
import yaml

def load_yaml_file(filename):
    with open(filename) as f:
        try:
            config = yaml.load(f)
            #print (config)
        except:
            raise IOError("load {0} error!".format(filename))
    return config


def add_arguments(parser):
    #data
    parser.add_argument("--train_file", type=str, default=None,
                        help="train file, should be .csv, .txt, .libsvm")
    parser.add_argument("--eval_file", type=str, default=None,
                        help="eval file, should be .csv, .txt, .libsvm")
    parser.add_argument("--infer_file", type=str, default=None,
                        help="infer file, should be .csv, .txt, .libsvm")
    parser.add_argument("--PAIR_NUM", type=int, default=None,
                        help="PAIR_NUM, should be int")
    parser.add_argument("--DNN_FIELD_NUM", type=int, default=None,
                        help="DNN_FIELD_NUM, should be int")
    parser.add_argument("--FEATURE_COUNT", type=int, default=None,
                        help="FEATURE_COUNT, should be int")
    parser.add_argument("--FIELD_COUNT", type=int, default=None,
                        help="FIELD_COUNT, should be int")
    parser.add_argument("--data_format", type=str, default=None,
                        help="data_format, should be str")
    #model
    parser.add_argument("--model_type", type=str, default=None,
                        help="model_type, should be str")
    parser.add_argument("--dim", type=int, default=None,
                        help="dim, should be int")
    parser.add_argument("--layer_sizes", type=str, default=None,
                        help="layer_sizes, should be str")
    parser.add_argument("--activation", type=str, default=None,
                        help="activation, should be str")
    parser.add_argument("--dropout", type=str, default=None,
                        help="dropout, should be str")
    parser.add_argument("--batch_norm", type=str, default=None,
                        help="batch_norm, should be str")
    parser.add_argument("--attention_layer_sizes", type=int, default=None,
                        help="attention_layer_sizes, should be int")
    parser.add_argument("--attention_activation", type=str, default=None,
                        help="attention_activation, should be str")
    parser.add_argument("--attention_dropout", type=float, default=None,
                        help="attention_dropout, should be float")
    parser.add_argument("--attention_batch_norm", type=int, default=None,
                        help="attention_batch_norm, should be int")

    #train
    parser.add_argument("--init_method", type=str, default=None,
                        help="init_method, should be str")
    parser.add_argument("--init_value", type=float, default=None,
                        help="init_value, should be float")
    parser.add_argument("--regular_type", type=str, default=None,
                        help="regular_type, should be str")
    parser.add_argument("--embed_l2", type=float, default=None,
                        help="embed_l2, should be float")
    parser.add_argument("--embed_l1", type=float, default=None,
                        help="embed_l1, should be float")
    parser.add_argument("--layer_l2", type=float, default=None,
                        help="layer_l2, should be float")
    parser.add_argument("--layer_l1", type=float, default=None,
                        help="layer_l1, should be float")
    parser.add_argument("--learning_rate", type=float, default=None,
                        help="learning_rate, should be float")
    parser.add_argument("--loss", type=str, default=None,
                        help="loss, should be str")
    parser.add_argument("--optimizer", type=str, default=None,
                        help="optimizer, should be str")
    parser.add_argument("--epochs", type=int, default=None,
                        help="epochs, should be int")
    parser.add_argument("--batch_size", type=int, default=None,
                        help="batch_size, should be int")

    #info
    parser.add_argument("--show_step", type=int, default=None,
                        help="show_step, should be int")
    parser.add_argument("--metrics", type=str, default=None,
                        help="metrics, should be str")

def run():
    #初始的yaml文件的路径
    INITIAL_PATH = './config/initial_network.yaml'
    initial_config = load_yaml_file(INITIAL_PATH)
    tool_parser = argparse.ArgumentParser()
    add_arguments(tool_parser)
    FLAGS, unparsed = tool_parser.parse_known_args()
    new_config = vars(FLAGS)

    cateList = ['data', 'model', 'train', 'info']
    for cate in cateList:
        for key, val in initial_config[cate].items():
            if key in new_config and new_config[key] is not None:
                initial_config[cate][key] = new_config[key]

    TUNED_PATH = './config/network.yaml'
    with open(TUNED_PATH, 'w') as outfile:
        yaml.dump(initial_config, outfile, default_flow_style=False)


run()