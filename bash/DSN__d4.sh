export PATH="/home/deeprec/usr/anaconda3/bin:$PATH" 
source /etc/profile
cd /mnt/ 
sudo mkdir jdlu_new 
sudo chmod 777 jdlu_new 
cd /mnt/jdlu_new 
mkdir codes 
cd /mnt/jdlu_new/codes/ 
sudo cp -r /tmp/bashfile/DSN/ /mnt/jdlu_new/codes/
sudo chmod 777 /mnt/jdlu_new/codes/ -R
cd /mnt/jdlu_new/codes/DSN/ 
sudo rm logs/* -rf 
cd data 
if [ -f train_9_7_0.2.txt ];
then echo train_file exist! ;
else /home/deeprec/bin/az storage blob download-batch -s https://zhfzhmsra.blob.core.windows.net/upload/ -d ./ --pattern train_9_7_0.2.txt ;
fi ;
cd .. 
cd data 
if [ -f test_9_7_0.2.txt ];
then echo eval_file exist! ;
else /home/deeprec/bin/az storage blob download-batch -s https://zhfzhmsra.blob.core.windows.net/upload/ -d ./ --pattern test_9_7_0.2.txt ;
fi ;
cd .. 
python rewrite_config.py --batch_size 1024 --dim 16 --eval_file data/test_9_7_0.2.txt --learning_rate 0.0001 --model_type dsnSingleChannel --train_file data/train_9_7_0.2.txt 
python main.py
echo Finished
