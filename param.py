PARAMS = {'model_param':
              {'--model_type': ['dsnSingleChannel'],  # Dsn, DSNSingleAttention, dsnSingleChannel, dssm
               '--dim': [8, 16, 32, 64, 128],
               '--learning_rate': [0.0001],
               '--train_file': ['data/train_9_7_0.2.txt'],
               '--eval_file': ['data/test_9_7_0.2.txt'],
               '--batch_size': [1024]
               }
          }
